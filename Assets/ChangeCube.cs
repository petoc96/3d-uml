﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeCube : MonoBehaviour {

    public bool toggle = false;
    public GameObject parent;
    public GameObject cubeObject;
    private GameObject frag;
    public Button button;
    public Material[] materials = new Material[2];

   /* public void setFragment(GameObject fragment) {
        this.fragment = fragment;
    }
    */
    public void changeMaterial() {
        toggle = !toggle;
        if (toggle)
        {
            //cubeObject.GetComponent<MeshRenderer>().material = materials[0];
            //Instantiate(fragment);
            frag = GameObject.CreatePrimitive(PrimitiveType.Cube);
            //frag.AddComponent<Rigidbody>();
            frag.transform.parent = GameObject.Find("Canvas").transform;
            frag.transform.localScale += new Vector3(100, 100, 100); 
            frag.transform.position = new Vector3(612, 339, -159);
        }
        else
        {
            Destroy(frag);
            //cubeObject.GetComponent<MeshRenderer>().material = materials[1];
        }
       

    }

	// Use this for initialization
	void Start () {

        Button btn1 = button.GetComponent<Button>();
        btn1.onClick.AddListener(changeMaterial);

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
